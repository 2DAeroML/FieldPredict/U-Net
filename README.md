# AirfoilMNIST-incompressible Example U-Net

This project demonstrates the use of a U-Net architecture for processing the AirfoilMNIST dataset. The U-Net model is adapted from the [Deep Flow Prediction repository](https://github.com/thunil/Deep-Flow-Prediction) and showcases the potential of deep learning in aerodynamic flow predictions.

## Requirements

To install the necessary requirements, run the following command:

```bash
pip install -r requirements.txt
```

## Dataset

The AirfoilMNIST-incompressible dataset can be obtained from the [2DAeroML GitLab repository](https://gitlab.lrz.de/2DAeroML). It is a new dataset designed for machine learning applications in two-dimensional aerodynamics.

## Usage

The new_UNet.py file contains the implementation of the U-Net model along with example usage. To run the example, simply execute the script:

```bash
python new_UNet.py
```

## Model Training

Currently, no pre-trained model is provided. Training the model from scratch took approximately 2 hours on a system equipped with four NVIDIA GTX 1080 GPUs.

## License

This project is licensed under the GPL v3.0 License - see the LICENSE file for details.

