# To do list:
# - important: create timestamp folder for all outputs, instead of saving everything in the current directory
# - important: use tf.keras.callback.ModelCheckpoint to safe model after every epoch
# - improvement: clean up code and add more comments
# - improvement: add error handling where usefull
# - minor: add additional logging where usefull
# - optional: check gpu usage and then use all available gpus with no load. Example: https://stackoverflow.com/questions/40069883/how-to-set-specific-gpu-in-tensorflow
# - optional: specify all settings in a loop for hyper parameter tuning and save a .json config file for every iteration

# Imports
import logging
import os
import time
from pathlib import Path
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import PercentFormatter
import tensorflow as tf
import tensorflow_datasets as tfds

# GPU setup
#os.environ["CUDA_VISIBLE_DEVICES"]="0, 1, 2, 3"

# Additional Startup
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'  # disable Tensorflow INFO and WARNING messages

# Setup logging
filename = 'scaling_' + time.strftime("%Y-%m-%d_%H-%M-%S")
logger = logging.getLogger(__name__)
logging.basicConfig(
    encoding= 'utf-8', 
    level = logging.INFO, 
    format = '%(asctime)s %(levelname)s: %(message)s', 
    datefmt='%Y-%m-%d %H:%M:%S', 
    handlers=[
        logging.FileHandler(f'{filename}.log'),
        logging.StreamHandler(),
    ],
)

# Create path to dataset location
dataset_path = Path('..') / 'nacaFOAM' / 'airfoilMNIST-incompressible_new_mach_split'

# Setup
epochs = 80
base_learning_rate = 0.0004
batch_size = 16 * 4 # Attention, check number of available gpus!
input_velocity_x_global_max = 0.25 
output_pressure_global_max = abs(-7.69933) 
output_velocity_x_global_max = 2.50531 
output_velocity_y_global_max = 2.80496 
inspect_dataset = True
offset = 1e-5
n_validation = 1000 #validation_number-1

# Plot setup
examples_to_visualize = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 113, 195]
#plt.rcParams['font.family'] = 'Nimbus Roman'
cm = 1/2.54 #centimeter to inches
fig_width = 20 * cm
font_size_title = 12
font_size_label = 10
font_size_ticks = 8
pressure_min = -3
pressure_max = 1
velocity_x_min = 0.5
velocity_x_max = 1.5
velocity_y_min = -0.7
velocity_y_max = 0.7
delta_pressure_min = -0.2
delta_pressure_max = 0.2
delta_velocity_x_min = -0.1
delta_velocity_x_max = 0.1
delta_velocity_y_min = -0.1
delta_velocity_y_max = 0.1
alpha = 1.0

# Create helper functions
def parse_dataset(element):
    # Extract the 'encoder' and 'decoder' features from the element
    encoder = element['encoder']
    output_pressure = element['decoder'][:, :, 0:1]
    output_velocity_x = element['decoder'][:, :, 1:2]
    output_velocity_y = element['decoder'][:, :, 2:3]

    # Resize the dataset to 128*128 to comply with the model architecture:
    encoder = tf.image.resize(encoder, [128, 128], method='nearest')
    output_pressure = tf.image.resize(output_pressure, [128, 128], method='nearest')
    output_velocity_x = tf.image.resize(output_velocity_x, [128, 128], method='nearest')
    output_velocity_y = tf.image.resize(output_velocity_y, [128, 128], method='nearest')

    # Get all positions in encoder that are not 0 and create mask to apply preprocessing only to these positions
    mask = tf.math.not_equal(encoder, 0)

    # Create the three input fields
    input_binary = tf.cast(tf.where(mask, 1, 0), tf.float32)
    input_velocity_x = encoder
    input_velocity_y = tf.cast(tf.where(mask, 0, 1), tf.float32) # inverse of input_binary, as no input_velocity_y is present in dataset

    # Calculate the inlet Mach number:
    masked_input_velocity_x = tf.boolean_mask(input_velocity_x, mask)
    mean_masked_input_velocity_x = tf.reduce_mean(masked_input_velocity_x) # Note: if input contains input_velocity_y unequal 0, then this should be considered here! 

    # Calculate the pressure coefficient:
    output_pressure = tf.where(mask, (output_pressure - 1) * (2 * 101325 / (1.225 * 1.402 * 287.0528 * 283.15)) / tf.square(mean_masked_input_velocity_x), 0) 
    
    # Input scaling with absolut maximum of each field    
    # input_binary = input_binary / 1 # not needed as maximum is already 1
    input_velocity_x = input_velocity_x / input_velocity_x_global_max
    #input_velocity_y = input_velocity_y / 1 # not needed as maximum is already 1
    
    # Output scaling with absolut maximum of each field
    output_pressure = tf.where(mask, output_pressure / output_pressure_global_max, output_pressure)
    output_velocity_x = tf.where(mask, output_velocity_x / output_velocity_x_global_max, output_velocity_x)
    output_velocity_y = tf.where(mask, output_velocity_y / output_velocity_y_global_max, output_velocity_y)

    # Concatenate the standardized fields back into a encoder and decoder
    encoder = tf.concat([input_binary, input_velocity_x, input_velocity_y], axis=-1)
    decoder = tf.concat([output_pressure, output_velocity_x, output_velocity_y], axis=-1)

    return encoder, decoder, mask, mean_masked_input_velocity_x

def unet_block(input_tensor, num_filters, kernel_size = 4, transposed = False, activation = 'relu', padding = 'same', batch_norm = True, dropout_rate = 0.01):
    """
    A function that creates a block for the U-Net architecture. 
    It first applies an activation function (ReLU or LeakyReLu) to the input tensor.
    Then, it applies either a Conv2D or a transposed convolution operation, depending on the 'transposed' argument.
    If 'batch_norm' is True, it applies batch normalization. If 'dropout_rate' is greater than 0, it applies dropout.
    The function return the resulting tensor.
    """
    if activation == 'leaky_relu':
        tensor = tf.keras.layers.LeakyReLU(alpha = 0.2)(input_tensor)
    else:
        tensor = tf.keras.layers.ReLU()(input_tensor)
    if not transposed:
        tensor = tf.keras.layers.Conv2D(num_filters, kernel_size, strides = 2, padding = padding)(tensor)
    else:
        tensor = tf.keras.layers.UpSampling2D((2, 2), interpolation = 'bilinear')(input_tensor)
        tensor = tf.keras.layers.Conv2D(num_filters, kernel_size-1, strides = 1, padding = padding)(tensor)
    if batch_norm:
        tensor = tf.keras.layers.BatchNormalization()(tensor)
    if dropout_rate > 0:
        tensor = tf.keras.layers.Dropout(dropout_rate)(tensor)
    return tensor

class CustomLoggingCallback(tf.keras.callbacks.Callback):
    def on_train_begin(self, logs=None):
        logger.info('Starting training')

#    def on_test_begin(self, logs=None):
#        logger.info('Starting testing')

    def on_predict_begin(self, logs=None):
        logger.info('Starting predicting')

    def on_train_end(self, logs=None):
        logger.info('Stopping training')

#    def on_test_end(self, logs=None):
#        logger.info('Stopping testing')

    def on_predict_end(self, logs=None):
        logger.info('Stopping predicting')

#    def on_epoch_begin(self, epoch, logs=None):
#        logger.info("Starting epoch {} of training".format(epoch+1))

    def on_epoch_end(self, epoch, logs=None):
        logger.info("Finishing epoch {} of training".format(epoch+1))

class CustomLRSchedule(tf.keras.optimizers.schedules.LearningRateSchedule):
    def __init__(self, total_steps, epochs, minLR, maxLR):
        super(CustomLRSchedule, self).__init__()
        self.total_steps = total_steps
        self.epochs = epochs
        self.minLR = minLR
        self.maxLR = maxLR
        self.steps_per_epoch = self.total_steps // self.epochs
        self.lr_rates = tf.Variable(self._precompute_learning_rates(), trainable=False)

    def _precompute_learning_rates(self):
        lr_rates = [self.maxLR if epoch < self.epochs // 2 else self._calc_learning_rate(epoch) for epoch in range(self.epochs)]
        return lr_rates

    def __call__(self, step):
        current_epoch = step // self.steps_per_epoch
        return tf.gather(self.lr_rates, current_epoch)

    def _calc_learning_rate(self, current_epoch):
        fmin = 0
        fmax = 6
        e = fmin + (current_epoch / self.epochs - 0.5) * 2 * (fmax - fmin)
        f = tf.math.pow(0.5, tf.cast(e, tf.float32))
        return self.minLR + (self.maxLR - self.minLR) * f

    def get_config(self):
        return {
            "total_steps": self.total_steps,
            "epochs": self.epochs,
            "minLR": self.minLR,
            "maxLR": self.maxLR
        }

# Create dataset builder
builder = tfds.builder_from_directory(dataset_path)

# Inspect dataset
train_number = builder.info.splits['train'].num_examples
validation_number = builder.info.splits['validation'].num_examples
overall_number = train_number + validation_number

if inspect_dataset == True:
    # Print info about dataset
    logger.info(f'Overall number of data samples: {overall_number}')
    logger.info(f'Train samples: {train_number}')
    logger.info(f'Validation samples: {validation_number}')
    batches_per_epoch = train_number//batch_size
    total_steps = train_number * batch_size
    logger.info(f'Batches per epoch: {batches_per_epoch}')
    logger.info('')

# Construct the Dataset pipeline for train
ds_train = builder.as_dataset(split='train',)
ds_train = ds_train.map(parse_dataset)
ds_train = ds_train.map(lambda encoder, decoder, mask, input_velocity: ({'input_layer': encoder, 'output_layer': decoder}, mask, input_velocity))
ds_train = ds_train.shuffle(1024, seed=0, reshuffle_each_iteration=True)
ds_train = ds_train.batch(batch_size=batch_size, drop_remainder=True, num_parallel_calls=tf.data.AUTOTUNE)
ds_train = ds_train.prefetch(tf.data.AUTOTUNE)

# Construct the Dataset pipeline for validation
ds_validation = builder.as_dataset(split='validation',)
ds_validation = ds_validation.map(parse_dataset)
ds_validation = ds_validation.map(lambda encoder, decoder, mask, input_velocity: ({'input_layer': encoder, 'output_layer': decoder}, mask, input_velocity))
ds_validation = ds_validation.batch(batch_size=batch_size//4, drop_remainder=True, num_parallel_calls=tf.data.AUTOTUNE)
ds_validation = ds_validation.prefetch(tf.data.AUTOTUNE)

# Plotting visualization
for i in examples_to_visualize:
    first_sample = next(iter(ds_validation.unbatch().skip(i).take(1)))
    input_output_dict, mask, input_velocity = first_sample
    first_encoder = input_output_dict['input_layer']
    first_decoder = input_output_dict['output_layer']
    first_encoder = first_encoder.numpy()
    first_decoder = first_decoder.numpy()

    # Create figure
    plt.figure(figsize=(fig_width, 0.5 * fig_width))

    # Subplot 1
    plt.subplot(2, 3, 1)
    plt.imshow(np.rot90(first_encoder[:, :, 0]), cmap='jet', vmin=0, vmax=1)
    plt.title('Binary')
    plt.ylabel('Input')
    plt.axis('off')
    plt.colorbar(location = 'right')

    # Subplot 2
    plt.subplot(2, 3, 2)
    plt.imshow(np.rot90(first_encoder[:, :, 1]), cmap='jet', vmin=0, vmax=1)
    plt.title('Inlet Velocity X')
    plt.axis('off')
    plt.colorbar(location = 'right')

    # Subplot 3
    plt.subplot(2, 3, 3)
    plt.imshow(np.rot90(first_encoder[:, :, 2]), cmap='jet', vmin=0, vmax=1)
    plt.title('Binary 2')
    plt.axis('off')
    plt.colorbar(location = 'right')

    # Subplot 4
    plt.subplot(2, 3, 4)
    plt.imshow(np.rot90(first_decoder[:, :, 0]), cmap='jet', vmin=-1, vmax=1)
    plt.title('Pressure')
    plt.ylabel('Output')
    plt.axis('off')
    plt.colorbar(location = 'right')

    # Subplot 5
    plt.subplot(2, 3, 5)
    plt.imshow(np.rot90(first_decoder[:, :, 1]), cmap='jet', vmin=-1, vmax=1)
    plt.title('Velocity X')
    plt.axis('off')
    plt.colorbar(location = 'right')

    # Subplot 6
    plt.subplot(2, 3, 6)
    plt.imshow(np.rot90(first_decoder[:, :, 2]), cmap='jet', vmin=-1, vmax=1)
    plt.title('Velocity Y')
    plt.axis('off')
    plt.colorbar(location = 'right')

    # Finishing off
    plt.tight_layout()
    plt.savefig(f'{filename}_visualization_{i}.png', dpi=500, bbox_inches='tight')
    logger.info(f'created {filename}_visualization_{i}.png')
    #plt.show()
    plt.close()
logger.info('')

# Create a MirroredStrategy.
logger.info('Starting model build')
strategy = tf.distribute.MirroredStrategy()
logger.info('Number of devices: {}'.format(strategy.num_replicas_in_sync))

# Get all callbacks
my_callbacks = [
    tf.keras.callbacks.ProgbarLogger(count_mode = 'samples'),
    CustomLoggingCallback(),
]

# Create the custom learning rate schedule
lr_schedule = CustomLRSchedule(total_steps, epochs, base_learning_rate * 0.1, base_learning_rate)

# Open a strategy scope.
with strategy.scope():
    channelExponent=6
    channels = int(2 ** channelExponent + 0.5)
    # Prepare model
    inputs = tf.keras.layers.Input(shape=(128, 128, 3), name = 'input_layer') 
    l1 = tf.keras.layers.Conv2D(channels, (4, 4), strides = 2, padding = 'same')(inputs) 
    l2   = unet_block(l1,  channels*2, activation = 'leaky_relu')
    l2b  = unet_block(l2,  channels*2, activation = 'leaky_relu')
    l3   = unet_block(l2b, channels*4, activation = 'leaky_relu')
    l4   = unet_block(l3,  channels*8, activation = 'leaky_relu')
    l5   = unet_block(l4,  channels*8, kernel_size = 2, padding = 'valid', activation = 'leaky_relu')
    l6   = unet_block(l5,  channels*8, kernel_size = 2, padding = 'valid', activation = 'leaky_relu', batch_norm = False)
    
    # note, kernel size internally reduced by one now
    dl6  = unet_block(l6,  channels*8, kernel_size = 2, padding = 'valid', transposed = True, activation = 'relu')
    dl5  = unet_block(tf.keras.layers.concatenate([dl6, l5]),  channels*8, kernel_size = 2, padding = 'valid', transposed = True, activation = 'relu')
    dl4  = unet_block(tf.keras.layers.concatenate([dl5, l4]),  channels*4, transposed = True)
    dl3  = unet_block(tf.keras.layers.concatenate([dl4, l3]),  channels*2, transposed = True)
    dl2b = unet_block(tf.keras.layers.concatenate([dl3, l2b]), channels*2, transposed = True)
    dl2  = unet_block(tf.keras.layers.concatenate([dl2b, l2]), channels*1, transposed = True)
    dl1b = tf.keras.layers.ReLU()(tf.keras.layers.concatenate([dl2, l1]))
    dl1 = tf.keras.layers.Conv2DTranspose(3, (4, 4), strides = 2, padding = 'same', name = 'output_layer')(dl1b)

    # Model setup
    model = tf.keras.models.Model(inputs=[inputs], outputs=[dl1])
    optimizer = tf.keras.optimizers.Adam(
        learning_rate = lr_schedule,
        beta_1 = 0.5,
        beta_2 = 0.999
    )
    model.compile(
        optimizer = optimizer, 
        loss = 'mean_absolute_error', 
        metrics=['mean_squared_error'], 
    )

# Finish model preparation
model.summary()
logger.info('Model build finished')
logger.info('')
#tf.keras.utils.plot_model(model, to_file='model.png', show_shapes = True)

# Model training
history = model.fit(
    ds_train.map(lambda data, mask, input_velocity_x: (data['input_layer'], data['output_layer'])),
    epochs = epochs, 
    verbose = 1,
    validation_data = ds_validation.map(lambda data, mask, input_velocity_x: (data['input_layer'], data['output_layer'])),
    callbacks = my_callbacks,
    )
pd.DataFrame(history.history).to_csv(f'{filename}_history.csv')
model.save(f'{filename}_model.keras')
logger.info(f'saved model {filename}_model.keras')

# Prepare training plots
history_dict = history.history
loss_values = history_dict['loss']
val_loss_values = history_dict['val_loss']
metric_values = history_dict['mean_squared_error']
val_metric_values = history_dict['val_mean_squared_error']
epochs = range(1, len(loss_values)+1)

# Plot training plots
plt.figure(figsize=(fig_width, 0.6 * fig_width))
plt.subplot(1, 2, 1)
plt.plot(epochs, loss_values, 'b', label = 'Training Loss')
plt.plot(epochs, val_loss_values, 'r', label = 'Validation Loss')
plt.title('Training & Validation Loss')
plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.legend()
plt.subplot(1, 2, 2)
plt.plot(epochs, metric_values, 'b', label = 'Training Metric')
plt.plot(epochs, val_metric_values, 'r', label = 'Validation Metric')
plt.title('Training & Validation Metric')
plt.xlabel('Epochs')
plt.ylabel('Metric')
plt.legend()
plt.tight_layout()
plt.savefig(f'{filename}_training.png', dpi=500, bbox_inches='tight')
logger.info(f'created {filename}_training.png')
#plt.show()
plt.close()
logger.info('Model training finished')
logger.info('')

# Prediction on validation data
logger.info('Starting predicting')
raw_predictions = model.predict(ds_validation.map(lambda data, mask, input_velocity_x: data['input_layer']), batch_size = batch_size/4, verbose = 1)
logger.info('Predicting finished')
logger.info('')

# Prepare Error computation
relative_error_pressure = []
relative_error_velocity_x = []
relative_error_velocity_y = []
relative_error_overall = []

# Plotting
for i in range(n_validation):
    logger.info(f'Computing relative error for sample {i+1} of {validation_number}')

    # Extract the ground truth and mask from dataset
    ground_truth_example = next(iter(ds_validation.unbatch().skip(i).take(1)))
    ground_truth_dict, mask, input_velocity_x = ground_truth_example
    ground_truth_decoder = ground_truth_dict['output_layer']

    # compute overlay to display geometry in black
    overlay = np.where(mask, np.nan, 0)
    
    # Extract the raw ground truth fields
    raw_ground_truth_pressure   = ground_truth_decoder[:, :, 0:1]
    raw_ground_truth_velocity_x = ground_truth_decoder[:, :, 1:2]
    raw_ground_truth_velocity_y = ground_truth_decoder[:, :, 2:3]

    # Extract the raw predicted fields
    raw_predicted_pressure   = raw_predictions[i, :, :, 0:1]
    raw_predicted_velocity_x = raw_predictions[i, :, :, 1:2]
    raw_predicted_velocity_y = raw_predictions[i, :, :, 2:3]

    # Inverse Transform the ground truth
    ground_truth_pressure  =  tf.where(mask, raw_ground_truth_pressure * output_pressure_global_max, 0)
    ground_truth_velocity_x = tf.where(mask, raw_ground_truth_velocity_x * output_velocity_x_global_max, 0)
    ground_truth_velocity_y = tf.where(mask, raw_ground_truth_velocity_y * output_velocity_y_global_max, 0)

    # Inverse Transform the prediction
    predicted_pressure  =  tf.where(mask, raw_predicted_pressure * output_pressure_global_max, 0)
    predicted_velocity_x = tf.where(mask, raw_predicted_velocity_x * output_velocity_x_global_max, 0)
    predicted_velocity_y = tf.where(mask, raw_predicted_velocity_y * output_velocity_y_global_max, 0)

    # Calculate error for each field
    relative_error_pressure_i = (tf.where(mask, tf.abs((predicted_pressure - ground_truth_pressure) / (ground_truth_pressure + offset)), 0).numpy()).mean()
    relative_error_velocity_x_i = (tf.where(mask, tf.abs((predicted_velocity_x - ground_truth_velocity_x) / (ground_truth_velocity_x + offset)), 0).numpy()).mean()
    relative_error_velocity_y_i = (tf.where(mask, tf.abs((predicted_velocity_y - ground_truth_velocity_y) / (ground_truth_velocity_y + offset)), 0).numpy()).mean()

    # Append results to error lists
    relative_error_pressure.append(relative_error_pressure_i)
    relative_error_velocity_x.append(relative_error_velocity_x_i)
    relative_error_velocity_y.append(relative_error_velocity_y_i)
    relative_error_overall.append((relative_error_pressure_i + relative_error_velocity_x_i + relative_error_velocity_y_i) / 3)

    # Check if this samples should be plotted
    if i in examples_to_visualize:
        # Output relative error for this sample
        logger.info(f'Pressure: Sample {i} with {relative_error_pressure_i*100}%')
        logger.info(f'Velocity X: Sample {i} with {relative_error_velocity_x_i*100}%')
        logger.info(f'Velocity Y: Sample {i} with {relative_error_velocity_y_i*100}%')
        logger.info(f'Overall: Sample {i} with {(relative_error_pressure_i + relative_error_velocity_x_i + relative_error_velocity_y_i) / 3*100}%')
        logger.info('')

        # Calculate delta for each field
        delta_pressure = predicted_pressure - ground_truth_pressure
        delta_velocity_x = predicted_velocity_x - ground_truth_velocity_x
        delta_velocity_y = predicted_velocity_y - ground_truth_velocity_y

        # Create figure
        plt.figure(figsize=(fig_width, 0.7 * fig_width))

        # Subplot 1
        plt.subplot(3, 3, 1)
        plt.imshow(np.rot90(ground_truth_pressure), cmap = 'jet', vmin = pressure_min, vmax = pressure_max)
        plt.title('Ground Truth')
        plt.ylabel('Pressure')
        plt.colorbar(location = 'left')
        plt.imshow(np.rot90(overlay), cmap = 'gray', alpha = alpha)
        plt.axis('off')

        # Subplot 2
        plt.subplot(3, 3, 2)
        plt.imshow(np.rot90(predicted_pressure), cmap='jet', vmin=pressure_min, vmax=pressure_max)
        plt.title('Prediction')
        plt.imshow(np.rot90(overlay), cmap = 'gray', alpha = alpha)
        plt.axis('off')

        # Subplot 3
        plt.subplot(3, 3, 3)
        plt.imshow(np.rot90(delta_pressure), cmap='seismic', vmin=delta_pressure_min, vmax=delta_pressure_max)
        plt.title('Delta')
        plt.colorbar(location = 'right')
        plt.axis('off')

        # Subplot 4
        plt.subplot(3, 3, 4)
        plt.imshow(np.rot90(ground_truth_velocity_x), cmap='jet', vmin=velocity_x_min, vmax=velocity_x_max)
        plt.ylabel('X-Velocity')
        plt.colorbar(location = 'left')
        plt.imshow(np.rot90(overlay), cmap = 'gray', alpha = alpha)
        plt.axis('off')

        # Subplot 5
        plt.subplot(3, 3, 5)
        plt.imshow(np.rot90(predicted_velocity_x), cmap='jet', vmin=velocity_x_min, vmax=velocity_x_max)
        plt.imshow(np.rot90(overlay), cmap = 'gray', alpha = alpha)
        plt.axis('off')

        # Subplot 6
        plt.subplot(3, 3, 6)
        plt.imshow(np.rot90(delta_velocity_x), cmap='seismic', vmin=delta_velocity_x_min, vmax=delta_velocity_x_max)
        plt.colorbar(location = 'right')
        plt.axis('off')

        # Subplot 7
        plt.subplot(3, 3, 7)
        plt.imshow(np.rot90(ground_truth_velocity_y), cmap='jet', vmin=velocity_y_min, vmax=velocity_y_max)
        plt.ylabel('Y-Velocity')
        plt.colorbar(location = 'left')
        plt.imshow(np.rot90(overlay), cmap = 'gray', alpha = alpha)
        plt.axis('off')

        # Subplot 8
        plt.subplot(3, 3, 8)
        plt.imshow(np.rot90(predicted_velocity_y), cmap='jet', vmin=velocity_y_min, vmax=velocity_y_max)
        plt.imshow(np.rot90(overlay), cmap = 'gray', alpha = alpha)
        plt.axis('off')

        # Subplot 9
        plt.subplot(3, 3, 9)
        plt.imshow(np.rot90(delta_velocity_y), cmap='seismic', vmin=delta_velocity_y_min, vmax=delta_velocity_y_max)
        plt.colorbar(location = 'right')
        plt.axis('off')

        plt.tight_layout()
        plt.savefig(f'{filename}_prediction_{i}.png', dpi=500, bbox_inches='tight')
        logger.info(f'created {filename}_prediction_{i}.png')
        #plt.show()
        plt.close()

logger.info('Error computation finished')
logger.info('')

# Get the minium relative error
relativ_error_pressure_min_i = np.argmin(relative_error_pressure)
relativ_error_velocity_x_min_i = np.argmin(relative_error_velocity_x)
relativ_error_velocity_y_min_i = np.argmin(relative_error_velocity_y)
relativ_error_overall_min_i = np.argmin(relative_error_overall)

# Print the minium relative error
logger.info('Minimum of relativ error on validation dataset:')
logger.info(f'Pressure: Sample {relativ_error_pressure_min_i} with {relative_error_pressure[relativ_error_pressure_min_i]*100}%')
logger.info(f'Velocity X: Sample {relativ_error_velocity_x_min_i} with {relative_error_velocity_x[relativ_error_velocity_x_min_i]*100}%')
logger.info(f'Velocity Y: Sample {relativ_error_velocity_y_min_i} with {relative_error_velocity_y[relativ_error_velocity_y_min_i]*100}%')
logger.info(f'Overall: Sample {relativ_error_overall_min_i} with {relative_error_overall[relativ_error_overall_min_i]*100}%')
logger.info('')

# Get the maxium relative error
relativ_error_pressure_max_i = np.argmax(relative_error_pressure)
relativ_error_velocity_x_max_i = np.argmax(relative_error_velocity_x)
relativ_error_velocity_y_max_i = np.argmax(relative_error_velocity_y)
relativ_error_overall_max_i = np.argmax(relative_error_overall)

# Print the maxium relative error
logger.info('Maximum of relativ error on validation dataset:')
logger.info(f'Pressure: Sample {relativ_error_pressure_max_i} with {relative_error_pressure[relativ_error_pressure_max_i]*100}%')
logger.info(f'Velocity X: Sample {relativ_error_velocity_x_max_i} with {relative_error_velocity_x[relativ_error_velocity_x_max_i]*100}%')
logger.info(f'Velocity Y: Sample {relativ_error_velocity_y_max_i} with {relative_error_velocity_y[relativ_error_velocity_y_max_i]*100}%')
logger.info(f'Overall: Sample {relativ_error_overall_max_i} with {relative_error_overall[relativ_error_overall_max_i]*100}%')
logger.info('')

# Calculating average error for validation dataset
average_relative_error_pressure = np.mean(relative_error_pressure)
average_relative_error_velocity_x = np.mean(relative_error_velocity_x)
average_relative_error_velocity_y = np.mean(relative_error_velocity_y)
average_relative_error_overall = np.mean(relative_error_overall)

# Printing average error
logger.info('Average of relativ error for validation dataset:')
logger.info(f'Pressure {average_relative_error_pressure*100}%')
logger.info(f'Velocity X {average_relative_error_velocity_x*100}%')
logger.info(f'Velocity Y {average_relative_error_velocity_y*100}%')
logger.info(f'Overall {average_relative_error_overall*100}%')
logger.info('')

# Calculating median error for validation dataset
median_relative_error_pressure = np.median(relative_error_pressure)
median_relative_error_velocity_x = np.median(relative_error_velocity_x)
median_relative_error_velocity_y = np.median(relative_error_velocity_y)
median_relative_error_overall = np.median(relative_error_overall)

# Printing median error
logger.info('Median of relativ error for validation dataset:')
logger.info(f'Pressure {median_relative_error_pressure*100}%')
logger.info(f'Velocity X {median_relative_error_velocity_x*100}%')
logger.info(f'Velocity Y {median_relative_error_velocity_y*100}%')
logger.info(f'Overall {median_relative_error_overall*100}%')
logger.info('')

# Plotting error histogram
plt.figure(figsize=(fig_width, 0.25 * fig_width))
plt.subplot(1, 4, 1)
plt.hist(
    np.array(relative_error_pressure)*100, 
    bins = 100, 
    range = [0, 100],
    color = 'black', 
    alpha = 0.9, 
)
plt.title('Pressure')
plt.xlabel('Rel. Error [%]')
plt.ylabel('Frequency [%]')
plt.xlim(0, 100)
plt.gca().yaxis.set_major_formatter(PercentFormatter(xmax=n_validation))
plt.grid(axis = 'y')
plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)
plt.subplot(1, 4, 2)
plt.hist(
    np.array(relative_error_velocity_x)*100, 
    bins = 100, 
    range = [0, 100], 
    color = 'black', 
    alpha = 0.9, 
)
plt.title('Velocity X')
plt.xlabel('Rel. Error [%]')
plt.xlim(0, 100)
plt.gca().yaxis.set_major_formatter(PercentFormatter(xmax=n_validation))
plt.grid(axis = 'y')
plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)
plt.subplot(1, 4, 3)
plt.hist(
    np.array(relative_error_velocity_y)*100,
    bins = 100, 
    range = [0, 100], 
    color = 'black', 
    alpha = 0.9, 
)
plt.title('Velocity Y')
plt.xlabel('Rel. Error [%]')
plt.xlim(0, 100)
plt.gca().yaxis.set_major_formatter(PercentFormatter(xmax=n_validation))
plt.grid(axis = 'y')
plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)
plt.subplot(1, 4, 4)
plt.hist(
    np.array(relative_error_overall)*100, 
    bins = 100, 
    range = [0, 100], 
    color = 'black', 
    alpha = 0.9, 
)
plt.title('Overall')
plt.xlabel('Rel. Error [%]')
plt.xlim(0, 100)
plt.gca().yaxis.set_major_formatter(PercentFormatter(xmax=n_validation))
plt.grid(axis = 'y')
plt.gca().spines['top'].set_visible(False)
plt.gca().spines['right'].set_visible(False)
plt.tight_layout()
plt.savefig(f'{filename}_error_histogram.png', dpi=500, bbox_inches='tight')
logger.info(f'created {filename}_error_histogram.png')
#plt.show()
plt.close()

logger.info('--------------- Done ---------------')